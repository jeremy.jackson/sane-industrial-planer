const expect = require('expect.js');

const UDP = require('../../src/udp-server/index');

const dgram = require('dgram');

describe('UDP server', () => {
  const udp = UDP({ type: 'udp6' });

  it('should inherit from dgram.Socket', () => {
    expect(udp instanceof dgram.Socket).to.be.ok();
  });
});
