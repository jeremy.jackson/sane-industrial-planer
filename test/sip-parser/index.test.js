const expect = require('expect.js');

const sipParser = require('../../src/sip-parser');

const sipMessage1 = [
  'INVITE sip:conf-fact@example.com SIP/2.0',
  'Content-Type: multipart/mixed;boundary="boundary1"',
  'Content-Length: 619',
  '',
  '--boundary1',
  'Content-Type: application/sdp',
  '',
  'v=0',
  'o=alice 2890844526 2890842807 IN IP4 atlanta.example.com',
  's=-',
  'c=IN IP4 192.0.2.1',
  't=0 0',
  'm=audio 20000 RTP/AVP 0',
  'a=rtpmap:0 PCMU/8000',
  'm=video 20002 RTP/AVP 31',
  'a=rtpmap:31 H261/90000',
  '',
  '--boundary1',
  'Content-Type: application/resource-lists+xml',
  'Content-Disposition: recipient-list',
  '',
  '<?xml version="1.0" encoding="UTF-8"?>',
  '<resource-lists xmlns="urn:ietf:params:xml:ns:resource-lists">',
  '  <list>',
  '    <entry uri="sip:bill@example.com"/>',
  '    <entry uri="sip:randy@example.net"/>',
  '    <entry uri="sip:joe@example.org"/>',
  '  </list>',
  '</resource-lists>',
  '--boundary1--',
].join('\r\n');

const sipMessage2 = [
  'INVITE sip:conf-fact@example.com SIP/2.0',
  'Content-Type: application/sdp',
  'Content-Length: 619',
  '',
  'v=0',
  'o=alice 2890844526 2890842807 IN IP4 atlanta.example.com',
  's=-',
  'c=IN IP4 192.0.2.1',
  't=0 0',
  'm=audio 20000 RTP/AVP 0',
  'a=rtpmap:0 PCMU/8000',
  'm=video 20002 RTP/AVP 31',
  'a=rtpmap:31 H261/90000',
].join('\r\n');


const rinfo = {
  address: '10.0.0.1',
  family: 'IPv4',
  port: 5060,
  size: 200,
};

describe('SIP Message Parser', () => {
  const parsed1 = sipParser(sipMessage1, rinfo);
  const parsed2 = sipParser(sipMessage2, rinfo);

  it('should have five properties', () => {
    expect(parsed1).to.have.property('original');
    expect(parsed1).to.have.property('method');
    expect(parsed1).to.have.property('startLine');
    expect(parsed1).to.have.property('messageHeader');
    expect(parsed1).to.have.property('body');
    expect(parsed1).to.have.property('rinfo');
  });

  it('should be equal to reference', () => {
    const reference1 = {
      original: 'INVITE sip:conf-fact@example.com SIP/2.0\r\nContent-Type: multipart/mixed;boundary="boundary1"\r\nContent-Length: 619\r\n\r\n--boundary1\r\nContent-Type: application/sdp\r\n\r\nv=0\r\no=alice 2890844526 2890842807 IN IP4 atlanta.example.com\r\ns=-\r\nc=IN IP4 192.0.2.1\r\nt=0 0\r\nm=audio 20000 RTP/AVP 0\r\na=rtpmap:0 PCMU/8000\r\nm=video 20002 RTP/AVP 31\r\na=rtpmap:31 H261/90000\r\n\r\n--boundary1\r\nContent-Type: application/resource-lists+xml\r\nContent-Disposition: recipient-list\r\n\r\n<?xml version="1.0" encoding="UTF-8"?>\r\n<resource-lists xmlns="urn:ietf:params:xml:ns:resource-lists">\r\n  <list>\r\n    <entry uri="sip:bill@example.com"/>\r\n    <entry uri="sip:randy@example.net"/>\r\n    <entry uri="sip:joe@example.org"/>\r\n  </list>\r\n</resource-lists>\r\n--boundary1--',
      method: 'INVITE',
      startLine: 'INVITE sip:conf-fact@example.com SIP/2.0',
      messageHeader: {
        'Content-Type': ['multipart/mixed', 'boundary="boundary1"'],
        'Content-Length': '619',
      },
      body: [
        {
          header: { 'Content-Type': 'application/sdp' },
          body: 'v=0\r\no=alice 2890844526 2890842807 IN IP4 atlanta.example.com\r\ns=-\r\nc=IN IP4 192.0.2.1\r\nt=0 0\r\nm=audio 20000 RTP/AVP 0\r\na=rtpmap:0 PCMU/8000\r\nm=video 20002 RTP/AVP 31\r\na=rtpmap:31 H261/90000\r\n',
        }, {
          header: {
            'Content-Type': 'application/resource-lists+xml',
            'Content-Disposition': 'recipient-list',
          },
          body: '<?xml version="1.0" encoding="UTF-8"?>\r\n<resource-lists xmlns="urn:ietf:params:xml:ns:resource-lists">\r\n  <list>\r\n    <entry uri="sip:bill@example.com"/>\r\n    <entry uri="sip:randy@example.net"/>\r\n    <entry uri="sip:joe@example.org"/>\r\n  </list>\r\n</resource-lists>',
        },
      ],
      rinfo: {
        address: '10.0.0.1',
        family: 'IPv4',
        port: 5060,
        size: 200,
      },
    };

    const reference2 = {
      original: 'INVITE sip:conf-fact@example.com SIP/2.0\r\nContent-Type: application/sdp\r\nContent-Length: 619\r\n\r\nv=0\r\no=alice 2890844526 2890842807 IN IP4 atlanta.example.com\r\ns=-\r\nc=IN IP4 192.0.2.1\r\nt=0 0\r\nm=audio 20000 RTP/AVP 0\r\na=rtpmap:0 PCMU/8000\r\nm=video 20002 RTP/AVP 31\r\na=rtpmap:31 H261/90000',
      method: 'INVITE',
      startLine: 'INVITE sip:conf-fact@example.com SIP/2.0',
      messageHeader: {
        'Content-Type': 'application/sdp',
        'Content-Length': '619',
      },
      body: 'v=0\r\no=alice 2890844526 2890842807 IN IP4 atlanta.example.com\r\ns=-\r\nc=IN IP4 192.0.2.1\r\nt=0 0\r\nm=audio 20000 RTP/AVP 0\r\na=rtpmap:0 PCMU/8000\r\nm=video 20002 RTP/AVP 31\r\na=rtpmap:31 H261/90000',
      rinfo: {
        address: '10.0.0.1',
        family: 'IPv4',
        port: 5060,
        size: 200,
      },
    };

    expect(parsed1).to.eql(reference1);
    expect(parsed2).to.eql(reference2);
  });
});
