const expect = require('expect.js');

const bodyParser = require('../../src/sip-parser/body-parser');

describe('Body Parser', () => {
  it('should produce output equal to reference', () => {
    const arrbody = [
      '--boundary1',
      'Content-Type: application/sdp',
      '',
      'v=0',
      'o=alice 2890844526 2890842807 IN IP4 atlanta.example.com',
      's=-',
      'c=IN IP4 192.0.2.1',
      't=0 0',
      'm=audio 20000 RTP/AVP 0',
      'a=rtpmap:0 PCMU/8000',
      'm=video 20002 RTP/AVP 31',
      'a=rtpmap:31 H261/90000',
      '',
      '--boundary1',
      'Content-Type: application/resource-lists+xml',
      'Content-Disposition: recipient-list',
      '',
      '<?xml version="1.0" encoding="UTF-8"?>',
      '<resource-lists xmlns="urn:ietf:params:xml:ns:resource-lists">',
      '  <list>',
      '    <entry uri="sip:bill@example.com"/>',
      '    <entry uri="sip:randy@example.net"/>',
      '    <entry uri="sip:joe@example.org"/>',
      '  </list>',
      '</resource-lists>',
      '--boundary1--',
    ];

    const boundary = /--boundary1( |\t)*/;

    const reference = [
      {
        header: { 'Content-Type': 'application/sdp' },
        body: 'v=0\r\no=alice 2890844526 2890842807 IN IP4 atlanta.example.com\r\ns=-\r\nc=IN IP4 192.0.2.1\r\nt=0 0\r\nm=audio 20000 RTP/AVP 0\r\na=rtpmap:0 PCMU/8000\r\nm=video 20002 RTP/AVP 31\r\na=rtpmap:31 H261/90000\r\n',
      },
      {
        header: {
          'Content-Type': 'application/resource-lists+xml',
          'Content-Disposition': 'recipient-list',
        },
        body: '<?xml version="1.0" encoding="UTF-8"?>\r\n<resource-lists xmlns="urn:ietf:params:xml:ns:resource-lists">\r\n  <list>\r\n    <entry uri="sip:bill@example.com"/>\r\n    <entry uri="sip:randy@example.net"/>\r\n    <entry uri="sip:joe@example.org"/>\r\n  </list>\r\n</resource-lists>',
      },
    ];

    const body = bodyParser(arrbody, boundary);
    expect(body).to.eql(reference);
  });
});
