const expect = require('expect.js');

const boundaryParser = require('../../src/sip-parser/boundary-parser');

const headerHasBoundary = {
  'Content-Type': ['multipart/mixed', 'boundary="boundary1"'],
  'Content-Length': '619',
};

const headerHasBoundaryCaseInsensitive = {
  'CoNteNt-TyPe': ['multipart/mixed', 'boundary="boundary1"'],
  'Content-Length': '619',
};

const headerNoMultipart = {
  'Content-Type': 'application/sdp',
  'Content-Length': '619',
};

const headerNoContentType = {
  'Content-Length': '619',
};

describe('Boundary Parser', () => {
  it('should return a RegExp if on the happy path', () => {
    const shouldBeRegexp = boundaryParser(headerHasBoundary);
    expect(shouldBeRegexp instanceof RegExp).to.be.ok();
  });

  it('should return a boundary pattern if present', () => {
    const pattern = boundaryParser(headerHasBoundary).toString();
    const patternCI = boundaryParser(headerHasBoundaryCaseInsensitive).toString();

    expect(pattern).to.be('/--boundary1( |\t)*?/');
    expect(patternCI).to.be('/--boundary1( |\t)*?/');
  });

  it('should return `undefined` if the `Content-Type` is not `multipart`', () => {
    const noMP = boundaryParser(headerNoMultipart);
    expect(noMP).to.be(undefined);
  });

  it('should return `undefined` if there is no `Content-Type` header', () => {
    const noCT = boundaryParser(headerNoContentType);
    expect(noCT).to.be(undefined);
  });
});
