/* eslint spaced-comment: "off" */
const expect = require('expect.js');

const dgram = require('dgram');

const SipEvents = require('../../src/sip-events');

describe('SIP Events Generation', () => {
  const mockRemoteClient = dgram.createSocket('udp4');
  const sipServer = new SipEvents({ udpOptions: { port: 5060 } }).setMaxListeners(20);
  sipServer.listen();
  console.log(`max listeners: ${sipServer.getMaxListeners()}`);

  /**** RFC 3261 Methods ****/
  const INVITE = [
    'INVITE sip:bob@biloxi.example.com SIP/2.0',
    'Via: SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bf9',
    'Max-Forwards: 70',
    'From: Alice <sip:alice@atlanta.example.com>;tag=9fxced76sl',
    'To: Bob <sip:bob@biloxi.example.com>',
    'Call-ID: 3848276298220188511@atlanta.example.com',
    'CSeq: 1 INVITE',
    'Contact: <sip:alice@client.atlanta.example.com;transport=tcp>',
    'Content-Type: application/sdp',
    'Content-Length: 151',
    '',
    'v=0',
    'o=alice 2890844526 2890844526 IN IP4 client.atlanta.example.com',
    's=-',
    'c=IN IP4 192.0.2.101',
    't=0 0',
    'm=audio 49172 RTP/AVP 0',
    'a=rtpmap:0 PCMU/8000',
  ].join('\r\n');

  let inviteCatch;
  sipServer.on('INVITE', (msg) => { inviteCatch = msg; });
  mockRemoteClient.send(INVITE, 5060);

  const ACK = [
    'ACK sip:bob@client.biloxi.example.com SIP/2.0',
    'Via: SIP/2.0/TCP client.atlanta.example.com:5060;branch=z9hG4bK74bd5',
    'Max-Forwards: 70',
    'From: Alice <sip:alice@atlanta.example.com>;tag=9fxced76sl',
    'To: Bob <sip:bob@biloxi.example.com>;tag=8321234356',
    'Call-ID: 3848276298220188511@atlanta.example.com',
    'CSeq: 1 ACK',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let ackCatch;
  sipServer.on('ACK', (msg) => { ackCatch = msg; });
  mockRemoteClient.send(ACK, 5060);

  const CANCEL = [
    'CANCEL sip:bob@biloxi.example.com SIP/2.0',
    'Via: SIP/2.0/UDP client.atlanta.example.com:5060;branch=z9hG4bK74bf9',
    'Max-Forwards: 70',
    'From: Alice <sip:alice@atlanta.example.com>;tag=9fxced76sl',
    'To: Bob <sip:bob@biloxi.example.com>',
    'Route: <sip:ss1.atlanta.example.com;lr>',
    'Call-ID: 2xTb9vxSit55XU7p8@atlanta.example.com',
    'CSeq: 1 CANCEL',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let cancelCatch;
  sipServer.on('CANCEL', (msg) => { cancelCatch = msg; });
  mockRemoteClient.send(CANCEL, 5060);

  const BYE = [
    'BYE sip:alice@client.atlanta.example.com SIP/2.0',
    'Via: SIP/2.0/TCP client.biloxi.example.com:5060;branch=z9hG4bKnashds7',
    'Max-Forwards: 70',
    'From: Bob <sip:bob@biloxi.example.com>;tag=8321234356',
    'To: Alice <sip:alice@atlanta.example.com>;tag=9fxced76sl',
    'Call-ID: 3848276298220188511@atlanta.example.com',
    'CSeq: 1 BYE',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let byeCatch;
  sipServer.on('BYE', (msg) => { byeCatch = msg; });
  mockRemoteClient.send(BYE, 5060);

  const OPTIONS = [
    'OPTIONS sip:carol@chicago.com SIP/2.0',
    'Via: SIP/2.0/UDP pc33.atlanta.com;branch=z9hG4bKhjhs8ass877',
    'Max-Forwards: 70',
    'To: <sip:carol@chicago.com>',
    'From: Alice <sip:alice@atlanta.com>;tag=1928301774',
    'Call-ID: a84b4c76e66710',
    'CSeq: 63104 OPTIONS',
    'Contact: <sip:alice@pc33.atlanta.com>',
    'Accept: application/sdp',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let optionsCatch;
  sipServer.on('OPTIONS', (msg) => { optionsCatch = msg; });
  mockRemoteClient.send(OPTIONS, 5060);

  const REGISTER = [
    'REGISTER sips:ss2.biloxi.example.com SIP/2.0',
    'Via: SIP/2.0/TLS client.biloxi.example.com:5061;branch=z9hG4bKnashds7',
    'Max-Forwards: 70',
    'From: Bob <sips:bob@biloxi.example.com>;tag=a73kszlfl',
    'To: Bob <sips:bob@biloxi.example.com>',
    'Call-ID: 1j9FpLxk3uxtm8tn@biloxi.example.com',
    'CSeq: 1 REGISTER',
    'Contact: <sips:bob@client.biloxi.example.com>',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let registerCatch;
  sipServer.on('REGISTER', (msg) => { registerCatch = msg; });
  mockRemoteClient.send(REGISTER, 5060);

  /**** RFC 3262 Methods ****/
  const PRACK = [
    'PRACK sip:222@10.129.45.104:5060 SIP/2.0',
    'Via: SIP/2.0/UDP 10.129.47.146:8000;branch=z9hG4bK3b1bce',
    '\t0-22330',
    'Max-Forwards: 70',
    'From: sip:111@10.129.47.146:8000;tag=48346074',
    'To: sip:222@10.129.45.104;tag=a94c095b773be1dd6e8d668a785a9c84904eaa2e',
    'Call-ID: 11408@10.129.47.146',
    'CSeq: 2 PRACK',
    'RAck: 1 1 INVITE',
    'Content-Type: application/sdp',
    'Content-Length: 174',
    '',
    'v=0',
    'o=_ 2890844527 2890844527 IN IP4 10.129.47.146',
    's=-',
    'c=IN IP4 10.129.47.146',
    't=0 0',
    'm=audio 9000 RTP/AVP 0 101',
    'a=rtpmap:0 PCMU/8000',
    'a=rtpmap:101 telephone-event/8000',
  ].join('\r\n');

  let prackCatch;
  sipServer.on('PRACK', (msg) => { prackCatch = msg; });
  mockRemoteClient.send(PRACK, 5060);

  /**** RFC 3311 Methods ****/
  const UPDATE = [
    'UPDATE sip:1234567890@10.1.1.1:5060;user=phone SIP/2.0',
    'Call-ID: 123456789abcdef@10.1.1.1:5060',
    'To: "1234567890"<sip:1234567890@10.1.1.1:5060;user=phone>',
    'From: "987654321" <sip:987654321@10.1.1.2;user=phone>',
    'CSeq: 26 UPDATE',
    'Allow: ACK, INVITE, BYE, CANCEL, REGISTER, REFER, OPTIONS, PRACK, INFO, UPDATE',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let updateCatch;
  sipServer.on('UPDATE', (msg) => { updateCatch = msg; });
  mockRemoteClient.send(UPDATE, 5060);

  /**** RFC 3428 Methods ****/
  const MESSAGE = [
    'MESSAGE sip:user2@domain.com SIP/2.0',
    'Via: SIP/2.0/TCP user1pc.domain.com;branch=z9hG4bK776sgdkse',
    'Max-Forwards: 70',
    'From: sip:user1@domain.com;tag=49583',
    'To: sip:user2@domain.com',
    'Call-ID: asd88asd77a@1.2.3.4',
    'CSeq: 1 MESSAGE',
    'Content-Type: text/plain',
    'Content-Length: 18',
    '',
    'Watson, come here.',
  ].join('\r\n');

  let messageCatch;
  sipServer.on('MESSAGE', (msg) => { messageCatch = msg; });
  mockRemoteClient.send(MESSAGE, 5060);

  /**** RFC 3515 Methods ****/
  const REFER = [
    'REFER sip:b@atlanta.example.com SIP/2.0',
    'Via: SIP/2.0/UDP agenta.atlanta.example.com;branch=z9hG4bK2293940223',
    'To: <sip:b@atlanta.example.com>',
    'From: <sip:a@atlanta.example.com>;tag=193402342',
    'Call-ID: 898234234@agenta.atlanta.example.com',
    'CSeq: 93809823 REFER',
    'Max-Forwards: 70',
    'Refer-To: (whatever URI)',
    'Contact: sip:a@atlanta.example.com',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let referCatch;
  sipServer.on('REFER', (msg) => { referCatch = msg; });
  mockRemoteClient.send(REFER, 5060);

  /**** RFC 3903 Methods ****/
  const PUBLISH = [
    'PUBLISH sip:presentity@example.com SIP/2.0',
    'Via: SIP/2.0/UDP pua.example.com;branch=z9hG4bK771ash02',
    'To: <sip:presentity@example.com>',
    'From: <sip:presentity@example.com>;tag=1234kljk',
    'Call-ID: 98798798@pua.example.com',
    'CSeq: 1 PUBLISH',
    'Max-Forwards: 70',
    'SIP-If-Match: dx200xyz',
    'Expires: 3600',
    'Event: presence',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let publishCatch;
  sipServer.on('PUBLISH', (msg) => { publishCatch = msg; });
  mockRemoteClient.send(PUBLISH, 5060);

  /**** RFC 6665 Methods ****/
  const SUBSCRIBE = [
    'SUBSCRIBE sip:+339012341234@anritsu-rcs.com;pres-list=rcs SIP/2.0',
    'Call-ID: 8Ih0yaUFAA@192.168.1.1',
    'CSeq: 1 SUBSCRIBE',
    'From: "test user" <sip:+339012341234@anritsu-rcs.com>;tag=8Ih0yaUGAA',
    'To: <sip:+339012341234@anritsu-rcs.com;pres-list=rcs>',
    'Via: SIP/2.0/UDP 192.168.1.1:5060;branch=z9hG4bKcad78c485b12e9ff2a8410e4d375722f383138;rport',
    'Max-Forwards: 70',
    'Route: <sip:192.168.1.2:5060;transport=udp;lr>',
    'Expires: 3600',
    'User-Agent: IM-client/OMA1.0 OrangeLabs-RCS-client/2.5.13',
    'Contact: <sip:192.168.1.1:5060;transport=UDP>;+sip.instance="<urn:gsma:imei:356432059050620>"',
    'Allow: INVITE,UPDATE,ACK,CANCEL,BYE,NOTIFY,OPTIONS,MESSAGE,REFER',
    'Event: presence',
    'Supported: eventlist',
    'Content-Length: 0',
    '',
  ].join('\r\n');

  let subscribeCatch;
  sipServer.on('SUBSCRIBE', (msg) => { subscribeCatch = msg; });
  mockRemoteClient.send(SUBSCRIBE, 5060);

  const NOTIFY = [
    'NOTIFY sip:+11234567890@ims.sharetechnote.com SIP/2.0',
    'Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bK-fa1fec3c8c092d3c03c8555617fa1730;transport=UDP',
    'Call-ID: 2489100824@192.168.1.15',
    'From: <sip:+11234567890@ims.sharetechnote.com>;tag=987654321',
    'To: <sip:+11234567890@ims.sharetechnote.com>;tag=3855569531',
    'Subscription-State: active;expires=3600',
    'Event: reg',
    'CSeq: 1 NOTIFY',
    'Contact: <sip:192.168.1.2:5060>',
    'Max-Forwards: 70',
    'Content-Type: application/reginfo+xml',
    'Content-Length: 336',
    '',
    '<reginfo xmlns="urn:ietf:params:xml:ns:reginfo" version="0" state="full">',
    '<registration aor="sip:+11234567890@ims.sharetechnote.com" id="12345" state="active">',
    '<contact id="100" state="active" event="registered" expires="3600">',
    '<uri>sip:+11234567890@192.168.1.15:5060</uri>',
    '</contact>',
    '</registration>',
    '</reginfo>',
  ].join('\r\n');

  let notifyCatch;
  sipServer.on('NOTIFY', (msg) => { notifyCatch = msg; });
  mockRemoteClient.send(NOTIFY, 5060);

  /**** RFC 6086 Methods ****/
  const INFO = [
    'INFO sip:alice@pc33.example.com SIP/2.0',
    'Via: SIP/2.0/UDP 192.0.2.2:5060;branch=z9hG4bKnabcdef',
    'To: Bob <sip:bob@example.com>;tag=a6c85cf',
    'From: Alice <sip:alice@example.com>;tag=1928301774',
    'Call-Id: a84b4c76e66710@pc33.example.com',
    'CSeq: 314333 INFO',
    'Info-Package: foo',
    'Content-type: application/foo',
    'Content-Disposition: Info-Package',
    'Content-length: 24',
    '',
    'I am a foo message type',
  ].join('\r\n');

  let infoCatch;
  sipServer.on('INFO', (msg) => { infoCatch = msg; });
  mockRemoteClient.send(INFO, 5060);

  /**** Custom Methods ****/
  const SPAM = [
    'SPAM sip:alice@pc33.example.com SIP/2.0',
    'Via: SIP/2.0/UDP 192.0.2.2:5060;branch=z9hG4bKnabcdef',
    'To: Bob <sip:bob@example.com>;tag=a6c85cf',
    'From: Alice <sip:alice@example.com>;tag=1928301774',
    'Call-Id: a84b4c76e66710@pc33.example.com',
    'CSeq: 314333 INFO',
    'Info-Package: foo',
    'Content-type: application/SPAM',
    'Content-Disposition: Info-Package',
    'Content-length: 24',
    '',
    'Lovely spam, wonderful spam...',
  ].join('\r\n');

  let spamCatch;
  sipServer.on('SPAM', (msg) => { spamCatch = msg; });
  mockRemoteClient.send(SPAM, 5060);

  /**** Disallowed Methods Have NO Listener ****/
  const IAMFAKE = [
    'IAMFAKE sip:alice@pc33.example.com SIP/2.0',
    'Via: SIP/2.0/UDP 192.0.2.2:5060;branch=z9hG4bKnabcdef',
    'To: Bob <sip:bob@example.com>;tag=a6c85cf',
    'From: Alice <sip:alice@example.com>;tag=1928301774',
    'Call-Id: a84b4c76e66710@pc33.example.com',
    'CSeq: 314333 INFO',
    'Info-Package: foo',
    'Content-type: application/SPAM',
    'Content-Disposition: Info-Package',
    'Content-length: 0',
    '',
  ].join('\r\n');

  let iamfakeCatch;
  sipServer.on('disallowed-method', (msg) => { iamfakeCatch = msg; });
  mockRemoteClient.send(IAMFAKE, 5060);

  // Checking these on the next tick will ensure that all of the
  // above listeners and sendings totally complete before we start
  // to evaluate their values.
  process.nextTick(() => {
    describe('RFC 3261 Events', () => {
      it('should emit INVITE', () => {
        expect(inviteCatch).to.be.ok();
      });

      it('should emit ACK', () => {
        expect(ackCatch).to.be.ok();
      });

      it('should emit CANCEL', () => {
        expect(cancelCatch).to.be.ok();
      });

      it('should emit BYE', () => {
        expect(byeCatch).to.be.ok();
      });

      it('should emit OPTIONS', () => {
        expect(optionsCatch).to.be.ok();
      });

      it('should emit REGISTER', () => {
        expect(registerCatch).to.be.ok();
      });
    });

    describe('RFC 3262 Methods', () => {
      it('should emit PRACK', () => {
        expect(prackCatch).to.be.ok();
      });
    });

    describe('RFC 3311 Methods', () => {
      it('should emit UPDATE', () => {
        expect(updateCatch).to.be.ok();
      });
    });

    describe('RFC 3428 Methods', () => {
      it('should emit MESSAGE', () => {
        expect(messageCatch).to.be.ok();
      });
    });

    describe('RFC 3515 Methods', () => {
      it('should emit REFER', () => {
        expect(referCatch).to.be.ok();
      });
    });

    describe('RFC 3903 Methods', () => {
      it('should emit PUBLISH', () => {
        expect(publishCatch).to.be.ok();
      });
    });

    describe('RFC 6665 Methods', () => {
      it('should emit SUBSCRIBE', () => {
        expect(subscribeCatch).to.be.ok();
      });

      it('should emit NOTIFY', () => {
        expect(notifyCatch).to.be.ok();
      });
    });

    describe('RFC 6086 Methods', () => {
      it('should emit INFO', () => {
        expect(infoCatch).to.be.ok();
      });
    });

    describe('Custom Methods', () => {
      it('should emit SPAM', () => {
        expect(spamCatch).to.be.ok();
      });
    });

    describe('Disallowed Methods Have No Named Listener', () => {
      it('should emit disallowed-method', () => {
        expect(iamfakeCatch).to.be.ok();
      });
    });
  });
});
