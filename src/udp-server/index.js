const dgram = require('dgram');

module.exports = (options) => {
  const type = options.type || 'udp4';
  const socket = dgram.createSocket(type);

  return socket;
};
