// Original header text:
// Content-Type: multipart/mixed;boundary="boundary1"
//
// Array value of the `Content-Type` header we're searching for
// and will ultimately be parsing here:
// [ 'multipart/mixed', 'boundary="boundary1"' ]

module.exports = (header) => {
  const ct = 'Content-Type'.toLowerCase();
  const keys = Object.keys(header);

  const ctIndex = keys.map(key => key.toLowerCase()).indexOf(ct);

  // If we have no Content-Type, return early.
  if (ctIndex < 0) return undefined;

  const ctKey = keys[ctIndex];

  // We only end up with an array if the original C-T header contained a `;`
  // character, which should ALWAYS happen when we have a `boundary` due to
  // the C-T being multipart.
  if (typeof header[ctKey] === 'string') return undefined;

  const boundary = header[ctKey]
    .filter(val => /^boundary=.*/.test(val))
    .map((val) => {
      const field = val.split('=')[1];
      return field.replace(/"/g, '');
    })[0];

  // https://tools.ietf.org/html/rfc2046#section-5.1.1
  // We're returning a RegExp pattern since there can be an arbitrary amount
  // of horizontal whitespace between the boundary word and the \r\n according
  // to the RFC.
  return new RegExp(`--${boundary}( |\t)*?`);
};
