const headerParser = require('./header-parser');
const boundaryParser = require('./boundary-parser');
const bodyParser = require('./body-parser');

module.exports = (msg, rinfo) => {
  const msgString = msg.toString();
  const lines = msgString.split('\r\n');

  const crlf = lines.findIndex(line => line === '');

  const startLine = lines[0];
  const method = startLine.split(' ')[0];

  const arrhead = lines.slice(1, crlf);
  const arrbody = lines.slice(crlf + 1);

  const messageHeader = headerParser(arrhead);

  const boundary = boundaryParser(messageHeader);

  let body;
  if (boundary === undefined) {
    body = arrbody.join('\r\n');
  } else {
    body = bodyParser(arrbody, boundary);
  }

  const output = {
    original: msgString,
    method,
    startLine,
    messageHeader,
    body,
    rinfo,
  };
  return output;
};
