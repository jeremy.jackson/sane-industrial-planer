module.exports = (header) => {
  const messageHeader = header
    .reduce((acc, cv) => {
      if (cv.startsWith('\t')) {
        acc[acc.length - 1] = acc[acc.length - 1] + cv;
      } else {
        acc.push(cv);
      }
      return acc;
    }, [])
    .map((line) => {
      const arr = line.split(/\s*?:(.+)/);
      const output = arr.map(x => x.trim());
      output[1] = output[1].split(/;/);
      if (output[1].length === 1) output[1] = output[1][0]; // eslint-disable-line
      return output;
    })
    .reduce((acc, cv) => {
      const [key, value] = cv;
      acc[key] = value;
      return acc;
    }, {});

  return messageHeader;
};
