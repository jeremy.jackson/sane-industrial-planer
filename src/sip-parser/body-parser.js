const headerParser = require('./header-parser');

module.exports = (arrbody, boundary) => {
  const boundaryLocations = arrbody
    .map((cv, ind) => {
      if (boundary.test(cv)) return ind;
      return undefined;
    })
    .filter(x => x !== undefined);

  const bodySlices = boundaryLocations
    .reduce((acc, topBoundary, ind, srcArray) => {
      const bottomBoundary = srcArray[ind + 1];

      if (bottomBoundary == null) return acc;

      acc.push([topBoundary + 1, bottomBoundary]);
      return acc;
    }, []);

  const bodyOutput = bodySlices
    .map(slices => arrbody.slice(slices[0], slices[1]))
    .reduce((acc, chunk) => {
      const crlf = chunk.findIndex(line => line === '');
      const head = chunk.slice(0, crlf);
      const body = chunk.slice(crlf + 1).join('\r\n');
      const header = headerParser(head);

      acc.push({ header, body });
      return acc;
    }, []);

  return bodyOutput;
};
