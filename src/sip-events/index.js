const EventEmitter = require('events');

const UDP = require('../udp-server');
const sipParser = require('../sip-parser');

module.exports = class extends EventEmitter {
  constructor(options) {
    super();

    const { udpOptions } = options;

    this.udpOptions = udpOptions;
    this.udpOptions.port = this.udpOptions.port || '5060';
    this.udpOptions.address = this.udpOptions.address || '127.0.0.1';

    this.udp = UDP(this.udpOptions);

    this.allowedMethods = [];
    this.on('newListener', () => {
      process.nextTick(() => {
        this.allowedMethods = this.eventNames();
      });
    });
    this.on('removeListener', () => {
      process.nextTick(() => {
        this.allowedMethods = this.eventNames();
      });
    });
  }

  listen() {
    this.listenUDP();
  }

  listenUDP() {
    this.udp.on('listening', () => {
      this.emit('udp-listening', this.udpOptions);

      this.udp.on('message', (msg, rinfo) => {
        const sipMessage = sipParser(msg, rinfo);
        if (this.allowedMethods.includes(sipMessage.method)) {
          this.emit(sipMessage.method, sipMessage, this.udp);
        } else {
          this.emit('disallowed-method', sipMessage, this.udp);
        }
      });
    });

    this.udp.on('error', (err) => {
      this.emit('udp-error', err);
    });

    this.udp.on('close', () => {
      this.emit('udp-close', this.udpOptions);
    });

    this.udp.bind(this.udpOptions);
  }

  close() {
    this.closeUDP();
  }

  closeUDP() {
    this.udp.close();
    this.emit('udp-close', this.udpOptions);
  }
};
