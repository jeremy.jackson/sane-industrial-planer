# Sane Industrial Planer, a SIP Messaging Server Library

[![pipeline status](https://gitlab.com/jeremy.jackson/sane-industrial-planer/badges/master/pipeline.svg)](https://gitlab.com/jeremy.jackson/sane-industrial-planer/commits/master)
[![coverage report](https://gitlab.com/jeremy.jackson/sane-industrial-planer/badges/master/coverage.svg)](https://gitlab.com/jeremy.jackson/sane-industrial-planer/commits/master)

Most SIP libraries on NPM focus on building SIP clients in the browser.  There's
really only one other SIP library that also functions as server independently of
some thrid-party service, however that one library does not really implement
itself as an `EventEmitter`. You could make it work like that, but I'd rather
have one that acts as an `EventEmitter` by design in order to create simplicity
and a consistent API.

## Design

After invoking this class, you can register a listener for each type of [SIP
Method](https://en.wikipedia.org/wiki/List_of_SIP_request_methods) you would
like to be listening for. You will also want to register a listener for
`disallowed-methods`. `disallowed-methods` is a catch-all for every SIP message
or method that you have not registered a listener for. You can also (optionally)
register a listener for a Custom SIP Method, however you must be aware that any
methods that are not backed by an RFC are unlikely to be accepted by other SIP
systems.

At the time of this writing, SIP only launches a UDP server for talking with the
outside world. Plans for a future version supporting TCP are in the works.

```
const SIP = require('sane-industrial-planer');
const handle = require('./my-custom-handlers');

const sipServer = new SIP();

sipServer.on('INVITE', (msg, sipSend) => {
  console.log('Received a SIP INVITE.');
  handle.invite(msg, sipSend);
});

sipServer.on('disallowed-method', (msg, sipSend) => {
  console.log(`Received a disallowed method ${msg.method}`);
  handle.disallowed(msg, sipSend);
});

// Automatically listens on `localhost:5060`.
sipServer.listen();
```

Make sure that when you're setting up your listeners that you take into account
the considerations in the `EventEmitter` [documentation on async vs
sync](https://nodejs.org/dist/latest-v10.x/docs/api/events.html#events_asynchronous_vs_synchronous)
and decide which way is appropriate for your particular application.

## Invoking

This exports a class inheriting from `EventEmitter`. Invoking it with defaults
causes it to listen for UDP on `localhost:5060`. You can invoke it with an
`Object` containing your options, which will be by category. Currently there are
only options that will be passed to the UDP server.

```
// Example using an `options` object.
// We're just passing options that are identical to the defaults
// to show what it should look like.
const SIP = require('sane-industrial-planer');

const sipOptions = {
  udpOptions: {
    port: 5060, // an integer
    address: 'localhost', // a string
  },
};

const sipServer = new SIP(sipOptions);
```

You **can not** modify these after invocation, though this might change in the
future so that can modify up until you start it with the `.listen()` method.

## Listeners

When you specify your listeners your callback function should take up to two
arguments. The first argument is an `Object` containing the entire,
mostly-parsed SIP message. The second argument is currently a wrapper for
`socket.send()` from the [`dgram`
module](https://nodejs.org/dist/latest-v10.x/docs/api/dgram.html#dgram_socket_send_msg_offset_length_port_address_callback).

```
sipServer.on('INVITE', (msg, sipSend) => {
  console.log(`SIP Message received: ${JSON.stringify(msg, null, 2)}`);

  const response = [
    'SIP/2.0 200 OK',
    'Via: SIP/2.0 server.example.com:5060;branch=z9hG4bK74bT6;received=127.0.0.1',
    'To: Bob <sips:bob@biloxi.example.com>;tag=02134',
    'From: Node Server <sips:node@server.example.com>;tag=56323',
    'Call-ID: 4802029847@biloxi.example.com',
    'CSeq: 1 NOTIFY',
    'Content-Length: 0',
    '', // RFC 3261 specifies the header block should be terminated by a blank line.
  ].join('\r\n');

  // Make sure you're following RFC 3261 Section 18 for deciding the port and
  // address to respond to. This library does not (currently) figure that out
  // for you.
  sipSend(response, port = msg.rinfo.port, address = msg.rinfo.address);
});
```

## Class Methods and Properties

### `udpOptions`

An Object containing the options passed to create the UDP server.

```
console.log(sipServer.udpOptions);
// { address: `127.0.0.1`, port: 5060 }
```

### `udp`

The UDP server instance. See the [dgram
documentation](https://nodejs.org/dist/latest-v10.x/docs/api/dgram.html) for
more details.

You can attach additional UDP listeners if you want to, but this is not
recommended.

### `allowedMethods`

An `Array` containing all of the listeners you've attached to the SIP server. We
also use this internally to determine which SIP messages end up going to the
`disallowed-method` listener.

```
sipServer.on('INVITE', () => { console.log('invite'); });
console.log(sipServer.allowedMethods);
// ['newListener', 'removeListener', 'INVITE']
// `newListener` and `removeListener` come from the EventEmitter class and we
// use them to keep `allowedMethods` up-to-date.
```

### `listen()`

Currently only starts the UDP server. Later, it will start both UDP and TCP
servers.

### `listenUDP()`

__Only__ starts the UDP server.

### `close()`

Currently only stops the UDP server. Later, it will stop both UDP and TCP
servers.

### `closeUDP()`

__Only__ starts the UDP server.

## SIP Message Parsing

Currently this library does very little parsing of SIP messages. It will break
out headers to an `Object`, but will not parse the body of a message according
to its `Content-Type`. Plans are in the works to have it parse `Content-Type:
application/sdp`, `Content-Type: application/rs-metadata`, and `Content-Type:
***/***+xml`.
